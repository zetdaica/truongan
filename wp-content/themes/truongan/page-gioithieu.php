
<?php  
    /**
     * Template name: Giới thiệu
     */
?>

<?php get_header();?>
    <!-- =====  CONTAINER START  ===== -->
    <div class="container">
      <div class="row ">
         <?php get_sidebar();?>
        <div id="column-right" class="col-sm-8 col-md-8 col-lg-9 mtb_30">
          <!-- =====  BANNER STRAT  ===== -->
          <div class="breadcrumb ptb_20">
            <h1> GIÓI THIỆU</h1>
          </div>
            <!-- Get post News Query -->
            <?php 
            $args = array(
                'post_type' => 'page',
                'post_status' => 'publish',
                'p' => 86,
            );
            $arr_posts = new WP_Query( $args );
            
            if ( $arr_posts->have_posts() ) :
            
                while ( $arr_posts->have_posts() ) :
                    $arr_posts->the_post();
                    ?>
                        <?php the_content(); ?>
                    <?php
                endwhile;
            endif;
        ?>
          <?php get_template_part('template-parts/brand') ?>
        </div>
      </div>
    
    </div>
    <!-- =====  CONTAINER END  ===== -->
    <!-- =====  FOOTER START  ===== -->
    <?php get_footer(); ?>
