
<?php  
    /**
     * Template name: Liên hệ
     */
?>

<?php get_header();?>
    <!-- =====  CONTAINER START  ===== -->
    <div class="container">
      <div class="row ">
         <?php get_sidebar();?>
        <div id="column-right" class="col-sm-8 col-md-8 col-lg-9 mtb_20">
          <!-- =====  BANNER STRAT  ===== -->
          <div class="breadcrumb ptb_20">
            <h1>Liên hệ</h1>
          </div>
          <div class="row">
            <div class="col-md-4 col-xs-12 contact">
              <div class="location mb_50">
                <h5 class="capitalize mb_20"><b> THÔNG TIN LIÊN HỆ</b></h5>
                <div class="address">
                   CÔNG TY TNHH NỘI-NGOẠI THẤT ÔTÔ TRƯỜNG AN
                  <br>511 - 513 -  An Dương Vương - Phường 8 - Quận 5 - Tp. HCM
                  <br>CN2: 538 An Dương Vương - Phường 9 - Quận 5 - Tp. HCM</div>
                <div class="call mt_10"><i class="fa fa-phone" aria-hidden="true"></i>028. 39381724 - 0918.029 627  (Mr. Bắc)</div>
              </div>
             
              <div class="Hello mb_50">
                <h5 class="capitalize mb_20">Liên hệ</h5>
                <div class="address">Vui lòng để lại thông tin cho chúng tôi</div>
                <div class="email mt_10"><i class="fa fa-envelope" aria-hidden="true"></i><a href="mailto:ototruongan@gmail.com" target="_top">ototruongan@gmail.com</a></div>
              </div>
            </div>
            <div class="col-md-8 col-xs-12 contact-form mb_50">
              <!-- Contact FORM -->
              <h5 class="capitalize mb_20"><b> THÔNG TIN KHÁCH HÀNG</b></h5>
              <?php 
                    $args = array(
                        'post_type' => 'page',
                        'post_status' => 'publish',
                        'p' => 22,
                    );
                     $arr_posts = new WP_Query( $args );
                    if ( $arr_posts->have_posts() ) :
                     
                        while ( $arr_posts->have_posts() ) : $arr_posts->the_post();?>
                                <?php the_content() ?>
                            <?php
                        endwhile;
                    endif;
                    wp_reset_query();
                ?>
               
              <!-- END Contact FORM -->
            </div>
          </div>
          <!-- map  -->
          <div class="row">
            <div class="col-xs-12 map mb_80">
              <div id="map" style="height: 100px">
                <img src="<?php  bloginfo('template_directory') ?>/images/map.png" alt="">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- =====  CONTAINER END  ===== -->
    <!-- =====  FOOTER START  ===== -->
    <?php get_footer(); ?>
