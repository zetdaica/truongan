<?php  
    /**
     * Template name: Tin tức
     */
?>

<?php get_header();?>
    <!-- =====  CONTAINER START  ===== -->
    <div class="container">
      <div class="row ">
         <?php get_sidebar();?>
         <div class="col-sm-8 col-md-8 col-lg-9 mtb_30">
          <!-- =====  BANNER STRAT  ===== -->
          <div class="breadcrumb ptb_20">
            <h1>TIN TỨC</h1>
            <!-- <ul>
              <li><a href="index.html">Home</a></li>
              <li class="active">Blog</li>
            </ul> -->
          </div>
          <!-- =====  BREADCRUMB END===== -->
          <div class="row">
            <div class="three-col-blog text-left">
                <?php 
                    $args = array(
                        'post_type' => 'post',
                        'post_status' => 'publish',
                        'category_name' => 'Tin tức',
                        'posts_per_page' => 5,
                    );
                    $arr_posts = new WP_Query( $args );
                    if ( $arr_posts->have_posts() ) :
                     
                        while ( $arr_posts->have_posts() ) : $arr_posts->the_post();?>
                                <div class="blog-item col-md-6 mb_30" style="height:500px">
                             
                                                <div class="post-format">
                                                <div class="thumb post-img">
                                                <a href="<?php the_permalink(); ?>"> 
                                                    <img style="width:100%;height:230px" src=" <?php the_post_thumbnail_url(array(500,200)); ?>"  alt="Tin tuc"></a></div>
                                                </div>
                                                <div class="post-info ">
                                                <h3 class="mb_10"> <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a> </h3>
                                                <p><?php the_excerpt(); ?></p>
                                                <div class="details ">
                                                    <div class="date pull-left"> <i class="fa fa-calendar" aria-hidden="true"></i> <?php echo get_the_date('d - m -Y'); ?></div>
                                                    <div class="more pull-right"> <a href="<?php the_permalink(); ?>">Xem chi tiết<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a></div>
                                                </div>
                                                </div>
                                            </div>
                                            

                            <?php
                        endwhile;
                    endif;
                    wp_reset_query();
                ?>
            </div>
          </div>
          <div class="pagination-nav text-center mtb_20">
            <ul>
              <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
              <li class="active"><a href="#">1</a></li>
              <li><a href="#">2</a></li>
              <li><a href="#">3</a></li>
              <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- =====  CONTAINER END  ===== -->
    <!-- =====  FOOTER START  ===== -->
<?php get_footer(); ?>


