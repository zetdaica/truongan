
<?php get_header();?>
    <!-- =====  CONTAINER START  ===== -->
    <div class="container">
      <div class="row ">
         <?php get_sidebar();?>
       
        <div class="col-sm-8 col-md-8 col-lg-9 mtb_10">
          <!-- =====  BANNER STRAT  ===== -->
          <div class="breadcrumb ptb_20">
            <h1> <?php the_category(); // lấy category của bài post này ?></h1>
          </div>
          <!-- =====  BREADCRUMB END===== -->
          <?php if (have_posts()) : ?>
              <?php while (have_posts()) : the_post(); ?>
          <div class="row">
            <div class="blog-item listing-effect col-md-12 mb_50">
              <div class="post-info mtb_20 ">
               
                <div class="details mtb_20">
                
                <div class="meta">
                  <h1><?php the_title(); // lấy tiều đề post ?></h1>
                 <div class="date"> <i class="fa fa-calendar" aria-hidden="true"></i> <?php echo get_the_date('d - m -Y'); ?> </div>
                </div>
                <p>
                  <?php the_content(); // lấy toàn bộ nội dung bài post ?>
                </p>
              
                  
               
                </div>
              <!-- <blockquote>consectetur adipiscing elit. In rutrum odio urna, vitae ultrices mi malesuada ut. Praesent lacus erat, ultricies ut risus nec, pellentesque interdum purus. In mi justo, consectetur tincidunt sapien eget, venenatis volutpat risus. Maecenas eget pretium eros. Integer tincidunt aliquet ligula in vulputate. Ut ut justo facilisis, vulputate augue vel, vestibulum tortor. Nullam varius lacus non porttitor sodales. Vivamus ultricies est vitae pharetra convallis. Sed suscipit, nisi sit amet tempus mollis, mauris ante tempor risu

              </blockquote> -->
              
             <div class="author-about mt_50">
                <h3 class="author-about-title">About the Author</h3>
                <div class="author mtb_30">
                  <div class="author-avatar"> <img alt="" src="images/user1.jpg"></div>
                  <div class="author-body">
                    <h5 class="author-name"><a href="#">Radley Lobortis</a></h5>
                    <div class="author-content mt_10">Vivamus imperdiet ex sed lobortis luctus. Aenean posuere nulla in turluctus. Aenean posuere nulla in tur pis porttitor laoreet. Quisque finibus aliquet purus. Ut et mi eu ante interdum .</div>
                  </div>
                </div>
              </div> 
              

              <div class="row">
            <div class="cmt">
              <div class="fb-comments" data-width="100%" data-href="<?php the_permalink(); ?>" data-numposts="3"></div>
            </div>
            
            <div id="fb-root"></div>
              <script>
              (function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.7&appId=750688268378229";
                fjs.parentNode.insertBefore(js, fjs);
              }(document, 'script', 'facebook-jssdk'));
              </script>
          </div>
          <?php endwhile;?>
                <?php endif; ?>
            </div>
          </div>
        
         
        </div>
        </div>
      </div>
      <?php get_template_part('template-parts/brand') ?>
    </div>
    <!-- =====  CONTAINER END  ===== -->
    <!-- =====  FOOTER START  ===== -->
    <?php get_footer(); ?>
