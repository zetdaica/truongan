<div id="column-left" class="col-sm-4 col-md-4 col-lg-3 ">
  <div id="category-menu" class="navbar collapse  mb_50 mb_40 hidden-sm-down in" aria-expanded="true" style="" role="button">
    <div class="nav-responsive">
      <ul class="nav  main-navigation collapse in ">
        <?php $args = array( 
                    'hide_empty' => 0,
                    'taxonomy' => 'noi_bat',
                    ); 
                    $cates = get_categories( $args ); 
                    foreach ( $cates as $cate ) {  ?>
        <li>
          <a href="<?php echo get_term_link($cate->slug, 'noi_bat'); ?>"><?php echo $cate->name ?></a>
        </li>
        <?php } ?>

        <?php $hang_xe = array( 
                    'hide_empty' => 0,
                    'taxonomy' => 'hang_xe',
                    ); 
                    $cates_hang_xe = get_categories( $hang_xe ); 
                    foreach ( $cates_hang_xe as $hang_xe ) {  ?>
        <li>
          <a href="<?php echo get_term_link($hang_xe->slug, 'hang_xe'); ?>"><?php echo $hang_xe->name ?></a>
        </li>
        <?php } ?>
      </ul>
    </div>
  </div>


  <div class="left_banner left-sidebar-widget mt_30 mb_30"> <a href="#"><img src="https://www.picclickimg.com/00/s/MTYwMFgxMjAw/z/D0wAAOSwPCVX4VgF/$/Texaco-Vintage-Gas-Station-Scene-Wall-Mural-Sign-_1.jpg" alt="Left Banner" class="img-responsive" /></a> </div>

  <div class="left-cms left-sidebar-widget mb_50">
    <ul>
      <li>
        <div class="feature-i-left ptb_40">
          <div class="icon-right Shipping"></div>
          <h6>Thời gian làm việc</h6>
          <p>Giờ : 8am - 11pm</p>
        </div>
      </li>
      <li>
        <div class="feature-i-left ptb_40">
          <div class="icon-right Order"></div>
          <h6>Hot Line</h6>
          <p>Gọi : 028. 39381724</p>
        </div>
      </li>
    </ul>
  </div>
  <div class="left-blog left-sidebar-widget mb_50">
    <div class="heading-part mb_20 ">
      <h2 class="main_title">TIN MỚI</h2>
    </div>
    <div id="left-blog">
      <div class="row ">
		  <?php $postquery = new WP_Query(array('posts_per_page' => 6));
		  if ($postquery->have_posts()) {
			 while ($postquery->have_posts()) : $postquery->the_post();
				$do_not_duplicate = $post->ID;
				?>
            <div class="blog-item mb_20">
              <div class="post-format col-xs-4">
                <div class="thumb post-img"><a href="<?php the_permalink(); ?>">
                    <img class="img-h65" src="<?php the_post_thumbnail_url(array(500,200)); ?>" alt="HealthCare">
                  </a></div>
              </div>
              <div class="post-info col-xs-8 ">
                <h5> <a href="<?php the_permalink(); ?>"><?php the_title();  ?> </a> </h5>
                <div class="date pull-left"> <i class="fa fa-calendar"
                                                aria-hidden="true"></i><?php echo get_the_date( 'd-m-Y'); ?> </div>
              </div>
            </div>
			 <?php endwhile;} ?>

      </div>
    </div>
  </div>
  <div class="Tags left-sidebar-widget mb_50">
    <div class="heading-part mb_20 ">
      <h2 class="main_title">Tags</h2>
    </div>
    <ul>
      <li><a href="#">kia</a></li>
      <li><a href="#">audi</a></li>
      <li><a href="#">lexus</a></li>
      <li><a href="#">CARENS</a></li>
      <li><a href="#">creative</a></li>
    </ul>
  </div>
</div>