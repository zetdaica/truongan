<div id="product-tab" class="mt_40">
  <div class="heading-part mb_20 ">
    <h2 class="main_title">Sản phẩm mới</h2>
  </div>
  <div class="tab-content clearfix box">
    <div class="tab-pane active" id="nArrivals">
      <div class="nArrivals owl-carousel">
		  <?php
		  //                    $args = array('post_type' => 'san_pham','meta_key' => 'trạng_thai','meta_value'	=> 'red',
		  //                    'tax_query' => array(
		  //                        array(
		  //                            'taxonomy' => 'hang_xe',
		  //                            'field' => 'slug',
		  //                            'terms' => array('audi','kia','lexus')
		  //                            ),
		  //                        )
		  //                    );

		  $args = array(
				'post_type' => 'san_pham',
				'meta_query' => array(
					 array(
						  'key' => 'trạng_thai',
						  'value' => 'Mới',
						  'compare' => 'LIKE'
					 )
				)
		  );

		  $arr_posts = new WP_Query($args);

		  if ($arr_posts->have_posts()) :
			 while ($arr_posts->have_posts()) : $arr_posts->the_post(); ?>
            <div class="product-grid">
              <div class="item">
                <div class="product-thumb">
                  <div class="image product-imageblock">
                    <a href="<?php the_permalink(); ?>">
                      <img class="product-home-new" data-name="product_image" src=" <?php the_post_thumbnail_url(); ?>"
                           alt="iPod Classic"
                           title="iPod Classic" class="img-responsive">
                      <img class="product-home-new" src=" <?php the_post_thumbnail_url(); ?>" alt="iPod Classic"
                           title="iPod Classic"
                           class="img-responsive">
                    </a>
                  </div>

                  <div class="caption product-detail text-left">
                    <h6 data-name="product_name" class="product-name mt_20">
                      <a href="<?php the_permalink(); ?>" title="Casual Shirt With Ruffle Hem">
								<?php the_title(); ?>
                      </a>
                    </h6>
                    <div class="rating">
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i
                                          class="fa fa-star fa-stack-1x"></i></span>
                      <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i
                            class="fa fa-star fa-stack-1x"></i></span>
                      <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i
                            class="fa fa-star fa-stack-1x"></i></span>
                      <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i
                            class="fa fa-star fa-stack-1x"></i></span>
                      <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i><i
                            class="fa fa-star fa-stack-x"></i></span>
                    </div>
                    <span class="price">
                      <b>Liên hệ 0918 029 627 </b><br>
                      <span>(Mr.Bắc)</span>
                    </span>
                  </div>
                </div>
              </div>
            </div>
			 <?php
			 endwhile;
		  endif;
		  wp_reset_query();
		  ?>
      </div>
    </div>

  </div>
</div>